import { Component, OnInit } from '@angular/core';
import {Usuario} from '../modelos';
import {ApiService} from '../ApiService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  pagina = 'A';
  usuario: Usuario = null;
  credencial = '';
  constructor(private apiservice: ApiService) { }

  ngOnInit(): void {
    this.usuario = {
      nombre: '',
      apellido: '',
      especialidad: '',
      codUni: '',
      correoUni: '',
      ciclo: null,
      tipoUsuario: '',
      descripcion: null,
      credencial: '',
    };
  }
  cambiarPagina(): void {
    // tslint:disable-next-line:triple-equals
    if (this.pagina == 'A') {
      this.pagina = 'R';
    } else {
      this.pagina = 'A';
    }
  }
  registrarUsuario(): void{
    console.log(this.usuario);
    this.apiservice.registrarUsuario(this.usuario).subscribe((data) => {
      console.log(data);
    });
  }
  autenticarUsuario(): void{
    console.log(this.usuario);
    this.apiservice.registrarUsuario(this.usuario).subscribe((data) => {
      console.log(data);
    });
  }
}
