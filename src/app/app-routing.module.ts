import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsesoriaComponent } from './asesoria/asesoria.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: '', component: AsesoriaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
