import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Inscripciones, Usuario} from './modelos';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService{
  baseurl = 'http://127.0.0.1:8080/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;charset=utf-8'
    })
  };
  // tslint:disable-next-line:typedef
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  constructor(private http: HttpClient) { }
  registrarUsuario(data: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseurl + 'registrarUsuario', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  autenticarUsuario(data: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseurl + 'autenticarUsuario', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
 
  crearInscripcion(data: Inscripciones ): Observable<Inscripciones> {
    return this.http.post<Inscripciones>(this.baseurl + 'crear-Inscripcion', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
}
