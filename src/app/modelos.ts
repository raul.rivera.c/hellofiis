export class Usuario {
  nombre: string;
  apellido: string;
  especialidad: string;
  codUni: string;
  correoUni: string;
  ciclo: number;
  tipoUsuario: string;
  descripcion: string;
  credencial: string;
}
export class Asesoria {
  linkAsesoria: string;
  codCurso:string;
  codAsesoria: number;
  tema: string;
  fechaAsesoria: string;
  horaInicio:string;
  horaFinal:string;
  capMin:number;
  capMax:number;
  codUsuarioCreacion:string;
  codUsuarioModificacion:string;
  descripcion:string;
  fechaInicioInscripcion:string;
  fechaFinalInscripcion:string;
  codAsesor:string;
}
 export class Inscripciones{
  codInscripcion:number;
  codAsesoria:number;
  codUni:string;
  fechaInscripcion:string
 }
