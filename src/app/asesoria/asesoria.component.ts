import { Component, OnInit } from '@angular/core';
import {ApiService} from '../ApiService';
import { Inscripciones } from '../modelos';
@Component({
  selector: 'app-asesoria',
  templateUrl: './asesoria.component.html',
  styleUrls: ['./asesoria.component.scss']
})
export class AsesoriaComponent implements OnInit {
  pagina:string = 'A';
  inscripcion:Inscripciones=null;
  asistencia:string='S';
  constructor(private apiservice: ApiService) { }

  ngOnInit(): void {
      this.inscripcion={
        codInscripcion:null,
        codAsesoria:null,
        codUni:"",
        fechaInscripcion:"21/02/2020"
      }
    }
  cambiarPagina():void{
    if(this.pagina == 'A'){
      this.pagina='N';
    }
    else{
      this.pagina='A';
    }
  }

  crearInscripcion():void{
    this.apiservice.crearInscripcion(this.inscripcion).subscribe((data)=> {
      console.log(data);
    });
    if(this.asistencia=='S'){
      this.asistencia='C';
    }
  }
  }

